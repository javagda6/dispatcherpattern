package com.sda.eventdispatcher.dispatcher;

import com.sun.istack.internal.NotNull;
import org.apache.commons.lang3.ClassUtils;

import java.util.*;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by amen on 12/4/17.
 */
public class EventDispatcher {
    private static final EventDispatcher instance = new EventDispatcher();

    private Map<Class<?>, List<Object>> map = new HashMap<>();
    //    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private ExecutorService executorService = Executors.newFixedThreadPool(3);

    public void register(Object o) {
        List<Class<?>> interfaces = ClassUtils.getAllInterfaces(o.getClass());
        for (Class<?> iinterface : interfaces) {
            // wszystkie obiekty ktore implementuja ten interfejs
            List<Object> objects = map.get(iinterface);

            if (objects == null) {
                objects = new ArrayList<>();
            }

            objects.add(o);
            map.put(iinterface, objects);
        }
    }

    public Optional<List> getAllObjectsImplementingInterface(Class<?> iinterface) {
        List<Object> list = map.get(iinterface);
        return Optional.ofNullable(list);
    }

    public void fireEvent(@NotNull IEvent event) {

//        executorService.submit(dispatcher);

        // bad
//        try {
//            executorService.submit(dispatcher);
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }

        //ok 1
        executorService.submit(() -> {
            try {
                event.run();
            } catch (Throwable e) {
                e.printStackTrace();
                System.err.println(e.getMessage());
            }
        });

//        executorService.submit(new Runnable() {
//            @Override
//            public void run() {
//                try{
//                    dispatcher.run();
//                }catch (Exception e){
//                    e.printStackTrace();
//                    System.err.println(e.getMessage());
//                }
//            }
//        });
    }

    private EventDispatcher() {
    }

    public static EventDispatcher getInstance() {
        return instance;
    }
}
