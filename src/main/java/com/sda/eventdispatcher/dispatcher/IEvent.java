package com.sda.eventdispatcher.dispatcher;

/**
 * Created by amen on 12/4/17.
 */
public interface IEvent /*extends Runnable*/{
    void run();
}
