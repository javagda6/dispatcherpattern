package com.sda.eventdispatcher.dispatcher.events;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.restaurant.Order;
import com.sda.eventdispatcher.dispatcher.IEvent;
import com.sda.eventdispatcher.listeners.OrderListener;

import java.util.List;
import java.util.Optional;

/**
 * Created by amen on 12/4/17.
 */
public class NewOrderEvent implements IEvent {

    private Order order;

    public NewOrderEvent(Order order) {
        this.order = order;
    }

    @Override
    public void run() {
        // optional - klasa która przechowuje wartość ale może być null'em
        Optional<List> objectList = EventDispatcher.getInstance().getAllObjectsImplementingInterface(OrderListener.class);

        // weryfikujemy czy obiekt posiada wartość (nie jest nullem) - wartość isPresent
        if(objectList.isPresent()) {
            // metoda get do wyciągnięcia z optionala wartości
            List<OrderListener> listeners = (List<OrderListener>) objectList.get();
            for (OrderListener listener : listeners) {
                listener.newOrder(order);
            }
        }else{
//            brak wartości w optionalu
        }
    }
}
