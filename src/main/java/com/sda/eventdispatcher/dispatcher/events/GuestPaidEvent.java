package com.sda.eventdispatcher.dispatcher.events;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.dispatcher.IEvent;
import com.sda.eventdispatcher.listeners.GuestPaidListener;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by amen on 12/4/17.
 */
public class GuestPaidEvent implements IEvent {
    private String guestName;

    public GuestPaidEvent(String guestName) {
        this.guestName = guestName;
    }

    @Override
    public void run() {
        Optional<List> objectList = EventDispatcher.getInstance().getAllObjectsImplementingInterface(GuestPaidListener.class);

        objectList.ifPresent(new Consumer<List>() {
            @Override
            public void accept(List list) {
                List<GuestPaidListener> listeners = (List<GuestPaidListener>) objectList.get();
                for (GuestPaidListener listener : listeners) {
                    listener.guestPaid(guestName);
                }
            }
        });
    }
}
