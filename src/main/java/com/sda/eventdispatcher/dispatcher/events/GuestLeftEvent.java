package com.sda.eventdispatcher.dispatcher.events;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.dispatcher.IEvent;
import com.sda.eventdispatcher.listeners.GuestLeftListener;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by amen on 12/4/17.
 */
public class GuestLeftEvent implements IEvent {
    private String guestName;

    public GuestLeftEvent(String guestName) {
        this.guestName = guestName;
    }

    @Override
    public void run() {
        try {
            Optional<List> objectList = EventDispatcher.getInstance().getAllObjectsImplementingInterface(GuestLeftListener.class);

            objectList.ifPresent(new Consumer<List>() {
                @Override
                public void accept(List list) {
                    List<GuestLeftListener> listeners = (List<GuestLeftListener>) objectList.get();
                    for (GuestLeftListener listener : listeners) {
                        listener.guestLeft(guestName);
                    }
                }
            });
        }catch (Exception e){

        }
    }
}
