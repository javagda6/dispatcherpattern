package com.sda.eventdispatcher.restaurant;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.listeners.NewGuestArrivedListener;
import com.sda.eventdispatcher.listeners.OrderListener;

/**
 * Created by amen on 12/4/17.
 */
public class Waiter implements NewGuestArrivedListener, OrderListener {
    private String name;

    public Waiter(String name) {
        this.name = name;
        EventDispatcher.getInstance().register(this);
    }

    @Override
    public void newGuestArrived(String guestName) {
        System.out.println("New guest arrived, " + name + " taking care of: "+ guestName);
    }

    @Override
    public void newOrder(Order what) {
        System.out.println(name + "informed about order: " + what);
    }
}
