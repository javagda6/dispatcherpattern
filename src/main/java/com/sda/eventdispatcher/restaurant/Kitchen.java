package com.sda.eventdispatcher.restaurant;

import com.sda.eventdispatcher.dispatcher.EventDispatcher;
import com.sda.eventdispatcher.listeners.OrderListener;

/**
 * Created by amen on 12/4/17.
 */
public class Kitchen implements OrderListener {
    public Kitchen() {
        EventDispatcher.getInstance().register(this);
    }

    @Override
    public void newOrder(Order what) {
        System.out.println("Kitchen informed about: " + what);
    }
}
