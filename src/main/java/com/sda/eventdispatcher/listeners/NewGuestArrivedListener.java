package com.sda.eventdispatcher.listeners;

/**
 * Created by amen on 12/4/17.
 */
public interface NewGuestArrivedListener {
    void newGuestArrived(String guestName);
}
